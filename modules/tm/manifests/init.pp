# Class: tm
#

class tm ($parameter_one = "default text") {
 file {'/tmp/test_module':
  ensure  => file,
  content => $parameter_one,
 }
}
